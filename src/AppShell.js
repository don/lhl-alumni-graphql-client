import React from 'react';
import { useManualQuery } from 'graphql-hooks';
import User from './User';

const USERS_QUERY = `query UsersQuery {
  users {
    first_name
    last_name
    name
    email
    admin
    theme_colour
  }
}
`;

const USER_QUERY = `query {
  user(id: 666) {
    id
    name
    email
    admin
    theme_colour
  }
}`;

export default function AppShell() {
  //const { loading, error, data } = useQuery(USERS_QUERY);
    const [getUser, {loading, error, data}] = useManualQuery(USER_QUERY);
    
    if (loading) return 'Loading......';
    if (error) return `WTF: ${JSON.stringify(error)}`;

    //No hooks after this point. (e.g. useEffect, useCallback)

    /*
    return (
      <div>
        {data.users.slice(0, 20).map(user => <User data={user} />)}
      </div>
    );
    */

    return (
      <div>
        <button onClick={getUser}>Load User</button>
        { loading && <div>Loading...</div> }
        { error && <div> ERRRRROOOORRR </div> }
        { data && data.user && <User data={data.user} /> }
      </div>
    );
  }

