import React, { useState } from 'react';
import styled from 'styled-components';
import { useMutation } from 'graphql-hooks';

const EMAIL_QUERY = `mutation UpdateEmail($id: ID!, $field: String!, $value: String!) {
  editUser(id: $id, field: $field, value: $value) {
    id
    name
    email
    admin
    theme_colour
  }
}`;

export default function User(props) {
  const [editing, setEditing] = useState(false);
  const [newEmail, setEmail] = useState(props.data.email);
  const [editUser] = useMutation(EMAIL_QUERY);
  const { id, name, email, admin, theme_colour } = props.data;

  const handleKeyUp = e => {
    if (e.keyCode === 13) {
      editUser({ variables: {id, field: 'email', value: newEmail}});
    }
  };

  return (
    <fieldset>
      <legend>{name}</legend>
      {editing ? 
        <input type="text" value={newEmail} onChange={e => setEmail(e.target.value)} onKeyUp={handleKeyUp} />
        : <H3>{email}</H3>
      }
      <p><strong>Admin?:</strong> {admin ? 'Y' : 'N'}</p>
      <p>Colour: {theme_colour}</p>
      <button onClick={() => setEditing(true)}>Edit</button>
    </fieldset>
  )
}

const H3 = styled.div`
  font-size: 1.2em;
`;
